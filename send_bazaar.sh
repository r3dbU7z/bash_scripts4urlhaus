#!/bin/bash

#Debug section
set -o errexit
#set -o nounset
set -o pipefail
#set -o xtrace 

#Separator
sprtr() {
echo $'\n'"================================================================"
}

if [ $# -lt 1 -o "$1" == "-h" -o "$1" == "--help" ]; then
	exec >&2
	sprtr
	echo "Usage: $0 [ -h | --help ]"
	echo "       $0 <cur_dir> -t tag -- Default tag: 'gafgyt' "
	echo "       $0  -d path -- Directory with samples"
	sprtr
	exit 0
fi

_bazaar_path="../PATH/TO/bazaar_upload.py"<- EDIT THIS 
_tag="gafgyt"
_dir_path=${PWD}

while getopts "t:d" opt
do
case $opt in

t) echo "Selected Tag: $OPTARG"
	_tag=$OPTARG
	;;
d) echo "Selected Path: $OPTARG"
	_dir_path=$OPTARG/
esac
done 

printf  "Selected Tag: $_tag\n"

_submit_samples(){

	for sample in $(ls); do
		${_bazaar_path} -t elf ${_tag} -f ${dir_path}${sample}
	done
	
}

_submit_samples

exit 0