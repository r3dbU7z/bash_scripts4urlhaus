#!/bin/sh

#Debug section
set -o errexit
#set -o nounset
set -o pipefail
#set -o xtrace

#Separator
sprtr() {
echo $'\n'"================================================================"
}

#Logs path
log_path="...urlhaus_logs..." <- EDIT THIS 
#URLhaus script path
urlhaus_path=./urlhaus.py
urlhaus=urlhaus.py

_tag=mirai

#IP_ADDR=$1

#Set variables
URL_ADDR=$(echo $1 | egrep -o '[f|ht]+tp://[^ ]+/')
URL_ADDR_HTTP=$(echo $1 | egrep -o '[f|ht]+tp://[^ ]+/' | sed 's/ftp/http/' )
IP_ADDR=$(echo $URL_ADDR | egrep -o '[f|ht]+tp://[^ ]+/' | awk -F'//' '{print $2}' | tr -d '/')

_dir_path=${PWD}/${_tag}_IP_$(echo ${IP_ADDR})

if [ ! -f "$urlhaus" ]; then
    echo "$urlhaus does not exist. Aborting execution"
	exit 0
fi

if [ $# -lt 1 -o "$1" == "-h" -o "$1" == "--help" ]; then
	exec >&2
	sprtr
	echo "Usage: $0 [ -h | --help ]"
	echo "       $0 <IP-address>" 

	sprtr
	exit 0
fi

echo "Selected tag: ${_tag}"

#Typical MIRAI filenames (maybe different)
n="x86 mips mpsl arm arm5 arm6 arm7 ppc m68k sh4 spc arc x64_86 "

for a in $n
do
        ./urlhaus.py -t elf ${_tag} -u ${URL_ADDR_HTTP}$a | 2>&1 tee -a\
			${log_path}/${_tag}/${_tag}_$(echo ${IP_ADDR}_date_$(date '+%Y-%m-%d')  | sed 's-:-_-').log
done


